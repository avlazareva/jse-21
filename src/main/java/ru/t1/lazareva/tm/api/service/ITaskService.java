package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name, String description);

    Task create(String userId, String name);

}